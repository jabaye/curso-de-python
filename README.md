
import csv
from pip._vendor.distlib.compat import raw_input
from tempfile import NamedTemporaryFile
import shutil

conteo = 0
def inicioMenu():
    print("<<<<--------- SELECCIONE UNA OPCIÓN --------->>>>")
    print("\t 1- Agregar nueva palabra")
    print("\t 2- Editar una palabra")
    print("\t 3- Ver listado completo")
    print("\t 4- Buscar significado")
    print("\t 5- Eliminar Registro")
    print("\t 9- Salir")

'''OPCION 1 AGREGAR PALABRA'''
def agregarPalabra():

    userList = []
    input_file = csv.DictReader(open('Palabras.csv'))
    for row in input_file:
        userList.append(row)

    print("Registro previos: "+str(len(userList)))

    while True:
        user = {}

        user["ID"] = int(raw_input("Ingresar ID, '0' para terminar: "))
        if user["ID"] == 0:
            print("Salistes del Sistema")
            break

        user["Palabra Incorrecta"] = raw_input("Ingresar nueva palabra del ghetto, 'fin' para terminar: ")
        if user["Palabra Incorrecta"].upper() == "fin".upper():
            print("Salistes del Sistema")
            break


        Incorrecto = ["xopa", "awebao", "ahuevao", "mopri", "llesca", "yesca", "chuzo", "compa", "chambon", "bulto",
                       "birria",
                       "chaniao", "tongo", "guabanazo", "biencuidao", "rakataka", "chacalita", "takillar", "taquillar",
                       "rantan", "liso", "lisa", "ponchera ", "vaina", "chiquishow", "cocoa", "cabreas", "joder",
                       "pollo", "polla","racataka"]

        Correcto = ["Que paso", "Estas pendejo", "vas como el huevo", "amigo o pasiero", "vamos a salir",
                               "vamos a salir",
                               "me acorde de algo", "compadre", "no tiene muchas habilidades", "falta de habilidades",
                               "jugar", "bien vestido", "policia", "golpe", "te cuido el carro", "chica de barrio",
                               "chica de barrio",
                               "ganar popularidad", "ganar popularidad", "hay mucho de algo", "atrevido", "atrevida",
                               "algo atrevido",
                               "alguna cosa", "hacer un espectaculo", "noticia importante", "molestas", "molestar",
                               "hombre", "mujer","chica de barrio"]

        Auxiliar = ""
        for i in range(len(Incorrecto)):
            if user['Palabra Incorrecta'].upper() == Incorrecto[i].upper():
                Auxiliar = Correcto[i]
                user['Palabra Correcta'] = Auxiliar
                print("si se encontro")
                #print(Auxiliar)
            else:
                print("")
        i += 1

        fDuplicate = False
        for userOld in userList:
            if (str(userOld ["ID"]) == str(user ["ID"])):
                fDuplicate = True
                print("Usuario Duplicado, Usuario NO agregado..!")

                break


        if fDuplicate == False:
            userList.append(user)
        #print(user)
        #print(userList)

    with open('Palabras.csv', 'w', newline='') as csvfile:
        #fieldnames = ['ID', 'Palabra Incorrecta', 'Palabra Correcta']
        fieldnames = userList[0].keys()
        w = csv.DictWriter(csvfile, fieldnames=fieldnames)
        w.writeheader()
        w.writerows(userList)

'''OPCION 2 EDITAR PALABRA'''
def editarListado():

    with open('Palabras.csv', newline='') as csvarchivo:
        entrada = csv.reader(csvarchivo)
        for reg in entrada:
            print({'ID': reg[0], 'Palabras Incorrecta': reg[1]})

    updatedlist = []
    with open("Palabras.csv", newline="") as f:
        reader = csv.reader(f)
        username = input("Ingrese el ID que desea Editar: ")
        for row in reader:
            if row[0] == username:
                userNew = raw_input("Ingresar nueva palabra del ghetto, 'fin' para terminar: ")
                if userNew.upper() == "fin".upper():
                    print("Salistes del Sistema")
                    break

        Incorrecto = ["xopa", "awebao", "ahuevao", "mopri", "llesca", "yesca", "chuzo", "compa", "chambon", "bulto",
                       "birria",
                       "chaniao", "tongo", "guabanazo", "biencuidao", "rakataka", "chacalita", "takillar", "taquillar",
                       "rantan", "liso", "lisa", "ponchera ", "vaina", "chiquishow", "cocoa", "cabreas", "joder",
                       "pollo", "polla","racataka"]

        Correcto = ["Que paso", "Estas pendejo", "vas como el huevo", "amigo o pasiero", "vamos a salir",
                               "vamos a salir",
                               "me acorde de algo", "compadre", "no tiene muchas habilidades", "falta de habilidades",
                               "jugar", "bien vestido", "policia", "golpe", "te cuido el carro", "chica de barrio",
                               "chica de barrio",
                               "ganar popularidad", "ganar popularidad", "hay mucho de algo", "atrevido", "atrevida",
                               "algo atrevido",
                               "alguna cosa", "hacer un espectaculo", "noticia importante", "molestas", "molestar",
                               "hombre", "mujer","chica de barrio"]

        Auxiliar = ""
        for i in range(len(Incorrecto)):
            if userNew.upper() == Incorrecto[i].upper():
                Auxiliar = Correcto[i]
                print("si se encontro")
                # print(Auxiliar)
            else:
                print("")
        i += 1

        filename = 'Palabras.csv'
        tempfile = NamedTemporaryFile(mode='w', delete=False, newline='')

        fields = ['ID', 'Palabra Incorrecta', 'Palabra Correcta']
        with open(filename, 'r') as csvfile, tempfile:
            reader = csv.DictReader(csvfile, fieldnames=fields)
            writer = csv.DictWriter(tempfile, fieldnames=fields)
            for row in reader:
                if row['ID'] == str(username):
                    print('Fila Actualizada', row ['ID'])
                    row['ID'], row['Palabra Incorrecta'], row['Palabra Correcta'] = username, userNew, Auxiliar
                writer.writerow(row)

        shutil.move(tempfile.name, filename)

'''OPCION 3 MOSTRAR LAS PALABRAS'''
def mostrarListado():

    with open('Palabras.csv', newline='') as csvarchivo:
        entrada = csv.reader(csvarchivo)
        for reg in entrada:
            print({'ID': reg[0], 'Palabras Incorrecta': reg[1]})

def buscarSignificado():
    with open('Palabras.csv', newline='') as csvarchivo:
        entrada = csv.reader(csvarchivo)
        for reg in entrada:
            print({'ID': reg[0], 'Palabras Incorrecta': reg[1]})

    updatedlist = []
    with open("Palabras.csv", newline="") as f:
        reader = csv.reader(f)
        username = input("Ingrese el ID que desea saber el significado: ")
        print("El significado de la palabra es....")
        for row in reader:
            if row[0] == username:
                print({'ID': row[0], 'Palabras Incorrecta': row[1], 'Palabras Correcta': row[2]})
                break

def seguir(num):
    os.system('cls')
    print("Desea continuar...")
    print("1. Si")
    print("2. No")
    cont = int(input("Escriba su respuesta:  \t"))
    if (cont == 1):
        num = 0
        print("entro en el 1")
    else:
        num = 1
        print("entra en el 2")
    return num

'''OPCION 5 ELIMINAR PALABRA'''
def eliminarPalabra():
    with open('Palabras.csv', newline='') as csvarchivo:
        entrada = csv.reader(csvarchivo)
        for reg in entrada:
            print(reg)


    updatedlist = []
    with open("Palabras.csv", newline="") as f:
        reader = csv.reader(f)
        username = input("Ingrese el ID que desea Eliminar: ")

        for row in reader:

            if row [0] != username:
                updatedlist.append(row)
        print(updatedlist)
        updatefile(updatedlist)

def updatefile(updatedlist):
    with open("Palabras.csv", "w", newline="") as f:
        Writer = csv.writer(f)
        Writer.writerows(updatedlist)
        print("El archivo ha sido actualizado")

num = 0
while num != 1:
    inicioMenu()
    ElegirOpcion = int(input("<< Elija una opcion: >> \t"))

    if ElegirOpcion == 1:
        print("<<< ELEGISTA AGREGAR UNA PALABRA >>> \t")
        agregarPalabra()
    elif ElegirOpcion == 2:
            print("<<< ELEGISTE EDITAR LISTADO >>>")
            editarListado()
    elif ElegirOpcion == 3:
            print("<<< ELEGISTE VER LISTADO >>>")
            mostrarListado()
    elif ElegirOpcion == 4:
            print("<<< ELEGISTE BUSCAR SIGNIFICADO >>>")
            buscarSignificado()
    elif ElegirOpcion == 5:
            print("<<< ELEGISTE ELIMINAR PALABRA >>>")
            eliminarPalabra()
    elif ElegirOpcion == 9:
        conex.close()
        break
    else:
        print("")
        input("No has pulsado ninguna opción correcta...\n pulsa una tecla para continuar")


Proyecto Final Python 